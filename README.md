## Ore Package Manger (OPM)
> An unofficial package manager for Sponge's Ore Plugin Respository.

### Installing
To install simply use yarn or npm:
```
yarn global add opm-cli
```
or
```
npm install -g opm-cli
```

### Usage
#### Initialization
To initialize a server run `opm init`. Then, when prompted select type of server and version you'd like to install.

#### Installing plugins
To install a plugin run `opm install <pluginId>` inside your server's root directory.

#### Getting info about a plugin
Run `opm info [pluginId]` to get info about a specific plugin, or `opm info` to list off all plugins (currently only lists first 25 plugins).

#### Configuring your server
Once initialized your servers root directory should contain a file named `omp.config.json`. Feel free to edit the `launchProperties` to tailor your needs.

#### Starting the server
Run `opm start` to start your server.
