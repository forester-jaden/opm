import axios from 'axios';
import * as fs from 'fs';

import buildPaths from '../util/build-path';

export default class Transport {
  url: string;

  constructor(url: string) {
    this.url = url;
  }

  async get(params: object = {}): Promise<any> {
    try {
      const { data } = await axios.get(this.url, { params });
      return data;
    } catch (error) {
      if (error.response) {
        if (error.response.status === 300) {
          return error.response.data;
        }

        throw new Error(`Error retreiving info: ${error.response.status} ${error.response.data}`);
      } else if (error.request) {
        throw new Error('Error retreiving info: Timed out.');
      } else {
        throw new Error(`Error retreiving info: ${error.message}`);
      }
    }
  }

  async save(method: string = 'POST', params: object = {}, output: string): Promise<void> {
    try {
      await buildPaths(output);

      const response = await axios({
        method: method,
        url: this.url,
        params,
        responseType: 'stream',
      });

      response.data.pipe(fs.createWriteStream(output));

      return new Promise<void>((resolve, reject) => {
        response.data.on('end', () => resolve());
        response.data.on('error', () => reject(new Error('Error downloading file.')));
      });
    } catch (error) {
      if (error.response) {
        throw new Error(`Error retreiving file: ${error.response.status} ${error.response.data}`);
      } else if (error.request) {
        throw new Error('Error retreiving file: Timed out.');
      } else {
        throw new Error(`Error retreiving file: ${error.message}`);
      }
    }
  }
}