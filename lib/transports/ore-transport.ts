import * as fs from 'fs';
import axios from 'axios';

import attemptJson from '../util/attempt-json';
import buildPaths from '../util/build-path';
import Transport from './transport';

export enum Endpoints {
  All,
  Project,
  Confirm,
  Custom
}

export type OreResponse = string | object | Array<object> | null;

export default class OreTransport extends Transport {
  constructor(endpoint: Endpoints, ...args: Array<string>) {
    const projectId = args[0] || '';
    const version = args[1] || '';
    switch (endpoint) {
      case Endpoints.Project:
        super(`https://ore.spongepowered.org/api/v1/projects/${projectId}`);
        break;
      case Endpoints.Confirm:
        super(`https://ore.spongepowered.org/api/projects/${projectId}/versions/${version}/download`);
        break;
      case Endpoints.Custom:
        super(projectId);
        break;
      default:
        super(`https://ore.spongepowered.org/api/v1/projects`);
        break;
    }
  }

  async get(params: object = {}): Promise<OreResponse> {
    return super.get(params);
  }

  async save(output: string): Promise<void> {
    return super.save('POST', {}, output);
  }
}
