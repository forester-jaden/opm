import Transport from "./transport";

export default class OreTransport extends Transport {
  async save(output: string): Promise<void> {
    return super.save('GET', {}, output);
  }
}