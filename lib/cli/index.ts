import * as program from 'commander';

import { version } from '../../package.json';
import info from './commands/info';
import install from './commands/install';
import init from './commands/init';
import start from './commands/start';
import list from '../util/list';

program
  .description('Install/update packages in current directory.')
  .version(version);

program
  .command('info [projectId]')
  .description('List info on all or specific Ore repositories.')
  .option('-c, --category <categories>', 'List of categories to search for.', list)
  .option('-s, --sort <sort>', 'How to sort the list of repos.', parseInt)
  .option('-l, --limit <limit>', 'How many entries to list.', parseInt)
  .option('-o, --offset <offset>', 'How many entries to skip.', parseInt)
  .option('-q, --query <query>', 'Search query.')
  .action(info);

program
  .command('init')
  .description('Setup a directory for a sponge server.')
  // .option('-s, --spongeVersion <spongeVersion>')
  // .option('-m, --minecraftVersion <mcVersion>')
  // .option('-l, --latest')
  // .option('-r, --recommended')
  .action(init);

program
  .command('install [projectId]')
  .description('Install an addon to your plugins directory.')
  .alias('i')
  .action(install);

program
  .command('start')
  .description('Start the server.')
  .alias('run')
  .action(start);

program.parse(process.argv);