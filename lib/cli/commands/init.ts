import * as inquirer from 'inquirer';
import * as path from 'path';

import SpongeTransport from "../../transports/sponge-transport";
import { startSpinner, stopSpinner, log } from "../../util/logger";
import getServer from '../../util/get-server';
import { getConfig, hasConfig } from '../../util/config';

interface InitParams {
  sponge?: string,
  minecraft?: string,
  latest?: boolean,
  recommended?: boolean,
}

interface InitAnswers {
  type: string,
  mcVersion: string
}

function promptType(): Promise<{ type: string }> {
  return inquirer.prompt({
    type: 'list',
    name: 'type',
    message: 'Type',
    choices: ['Vanilla', 'Forge', new inquirer.Separator(), 'Vanilla-Stable', 'Forge-Stable']
  });
}

function getServerName(serverType: string): string {
  return serverType.includes('Vanilla') ? 'spongevanilla' : 'spongeforge';
}

function shouldAskVersion(serverType: string): boolean {
  return serverType === 'Vanilla' || serverType === 'Forge';
}

function getServerInfo(serverType: string): Promise<SpongeInfo> {
  const serverName: string = getServerName(serverType);
  const transport: SpongeTransport = new SpongeTransport(`https://dl-api.spongepowered.org/v1/org.spongepowered/${serverName}`);

  return transport.get();
}

function promptMinecraftVersion(spongeInfo: SpongeInfo): Promise<{ mcVersion: string }> {
  return inquirer.prompt({
    type: 'list',
    name: 'mcVersion',
    message: 'Minecraft Version',
    choices: spongeInfo.dependencies.minecraft
  });
}

function getDownloads(serverType: string, mcVersion: string): Promise<SpongeDownloads> {
  const serverName: string = getServerName(serverType);
  const transport: SpongeTransport = new SpongeTransport(`https://dl-api.spongepowered.org/v1/org.spongepowered/${serverName}/downloads`);

  return transport.get({
    type: 'stable',
    minecraft: mcVersion,
    changelog: false,
  });
}

function promptSpongeVersion(downloads: SpongeDownloads): Promise<{ serverVersion: string }> {
  return inquirer.prompt({
    type: 'list',
    name: 'serverVersion',
    message: 'Server Version',
    choices: downloads.map(download => download.version)
  });
}

function downloadArtifact(serverType: string, mcVersion: string, spongeVersion: string): Promise<void> {
  const serverName = getServerName(serverType);
  const fileName = `${serverName}-${spongeVersion}.jar`;
  const artifact: string = `https://repo.spongepowered.org/maven/org/spongepowered/${serverName}/${spongeVersion}/${fileName}`;
  const transport: SpongeTransport = new SpongeTransport(artifact);

  return transport.save(path.resolve(process.cwd(), `./${fileName}`));
}

export default async function init(arg: string = '', cmd: InitParams): Promise<void> {
  if (await getServer() != null) {
    if (hasConfig()) {
      return console.log('Already contains a server. Run \'opm start\' to run server.');
    }
    await getConfig();
    return console.log('Server artifact already exists but no config exists. Config was created.');
  }

  try {
    console.log('Create a sponge server...');
  
    const { type: serverType } = await promptType();
    let mcVersion: string = '';
    let spongeVersion: string = '';
  
    startSpinner('Resolving server info...');
  
    const spongeInfo: SpongeInfo = await getServerInfo(serverType);
  
    stopSpinner();
  
    if (shouldAskVersion(serverType)) {
      mcVersion = (await promptMinecraftVersion(spongeInfo)).mcVersion;
  
      const downloads: SpongeDownloads = await getDownloads(serverType, mcVersion);
  
      spongeVersion = (await promptSpongeVersion(downloads)).serverVersion;
    } else {
      mcVersion = spongeInfo.buildTypes.stable.recommended.dependencies.minecraft;
      spongeVersion = spongeInfo.buildTypes.stable.recommended.version;
    }
  
    startSpinner('Downloading artifact...');
  
    await downloadArtifact(serverType, mcVersion, spongeVersion);
  
    stopSpinner();

    startSpinner('Creating config...');

    await getConfig();
  
    log('Successfully downloaded artifact. Run \'opm start\' to start the server.');
  } catch (error) {
    log(`Unable to download sponge. ${error.message}`);
  }
}
