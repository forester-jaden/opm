import * as shell from 'shelljs';
import * as repl from 'repl';

import getServer from '../../util/get-server';
import { ChildProcess } from 'child_process';
import { Config, getConfig } from '../../util/config';

let child: ChildProcess | null = null;
let server: repl.REPLServer | null = null;

function evaluate(cmd: string): void {
  if (child != null) {
    child.stdin.write(cmd);
  }
}

export default async function start(): Promise<void> {
  const jarFile: string | null = await getServer();
  if (jarFile != null) {
    const { launchProperties } = await getConfig();

    const execString: string = `java -Xms${launchProperties.minRam} -Xmx${launchProperties.maxRam} -jar ${jarFile}`;

    child = shell.exec(execString, { async: true }, (code) => {
      if (server != null) {
        if (child != null) {
          child.kill();
          child = null;
        }
        server.close();
      }
    }) as ChildProcess;

    server = repl.start({
      prompt: '> ',
      eval: evaluate,
    });
    server.on('exit', () => {
      if (child != null && !child.killed) {
        console.log('Closing server. Force killing in 3 seconds.');

        child.stdin.write('stop');

        setTimeout(() => {
          if (child != null) {
            child.kill();
          }
        }, 3000);
      }
    })
  } else {
    console.log('Unable to find server.')
  }
}