import * as fs from 'fs';
import * as path from 'path';
import * as inquirer from 'inquirer';

import { log, startSpinner, stopSpinner } from '../../util/logger';
import OreTransport, { Endpoints } from "../../transports/ore-transport";
import { addPlugin, Config, getConfig } from '../../util/config';

function getConfirmation(pluginId: string, version: string = 'recommended'): Promise<OreConfirmation> {
  const transport = new OreTransport(Endpoints.Confirm, pluginId, version);
  return <Promise<OreConfirmation>> transport.get();
}

function downloadFile(url: string, output: string): Promise<void> {
  const transport = new OreTransport(Endpoints.Custom, url);
  return transport.save(output);
}

async function installSingle(pluginPath: string, plugin: string, version: string, shouldSpin: boolean = true): Promise<void> {
  if (shouldSpin) {
    startSpinner(`Verifying ${plugin}...`);
  }
  const { post } = await getConfirmation(plugin);

  if (shouldSpin) {
    stopSpinner();
  }

  if (shouldSpin) {
    const answer = await inquirer.prompt({
      type: 'confirm',
      name: 'Confirm download of unverified plugin',
      message: `${plugin} is not verified by sponge. Install at your own risk. Continue?`,
      default: true,
    });
  
    if (!answer) {
      return log('Disclaimer declined. Cancelling install.');
    }
  }

  if (shouldSpin) {
    startSpinner(`Installing ${plugin}...`);
  }
  await downloadFile(post, pluginPath);

  if (shouldSpin) {
    stopSpinner();
  }

  if (shouldSpin) {
    startSpinner('Updating config...');
    await addPlugin(plugin ,version);
  }

  log(`Successfully installed ${plugin}@${version}.`, shouldSpin);
}

export default async function install(pluginId: string = '', cmd: object = {}): Promise<void> {
  const modsPath: string = path.resolve(process.cwd(), './mods');

  if (fs.existsSync(modsPath)) {
    try {
      if (pluginId !== '') {
        const [ plugin = '', version = 'recommended' ] = pluginId.split('@');
        const pluginPath: string = path.join(modsPath, 'plugins', `${plugin}.jar`);

        await installSingle(pluginPath, plugin, version);
      } else {
        startSpinner('Reading config...');
        const config: Config = await getConfig();

        stopSpinner();

        if (config.plugins) {
          const answer = await inquirer.prompt({
            type: 'confirm',
            name: 'Confirm download of unverified plugin',
            message: `Some plugins may not be verified by sponge. Install at your own risk. Continue?`,
            default: true,
          });
    
          if (!answer) {
            return log('Disclaimer declined. Cancelling install.');
          }

          startSpinner('Installing plugins...');
          const installs: Array<Promise<void>> = Object.keys(config.plugins).map(async (plugin) => {
            const version: string = config.plugins ? config.plugins[plugin] : 'recommended';
            const pluginPath: string = path.join(modsPath, 'plugins', `${plugin}.jar`);

            await installSingle(pluginPath, plugin, version, false);
          });
          await Promise.all(installs);

          log(`Successfully installed ${Object.keys(config.plugins).length} plugins.`);
        }
      }
    } catch (error) {
      log(`Unable to retreive plugin(s). ${error.message}`);
    }
  } else {
    log('Must be in a valid sponge server directory.');
  }
}
