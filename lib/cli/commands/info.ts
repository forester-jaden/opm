import OreTransport, { Endpoints } from "../../transports/ore-transport";
import { startSpinner, log } from "../../util/logger";

export interface InfoParams {
  categories?: Array<number>,
  sort?: number,
  query?: string,
  limit?: number,
  offset?: number,
}

export type InfoResponse = Array<OrePluginInfo> | OrePluginInfo;

function beautifyInfo(results: InfoResponse): string {
  if (results instanceof Array) {
    return results.map(result => (
      `${result.name} v${result.recommended.name} (${result.pluginId})`
    )).join('\n');
  }

  return `${results.name} v${results.recommended.name} (${results.pluginId})`;
}

async function getInfo(pluginId?: string, params?: OreSearchParams): Promise<OrePluginInfo | Array<OrePluginInfo>> {
  const transport: OreTransport = new OreTransport(pluginId ? Endpoints.Project : Endpoints.All, pluginId || '');

  if (pluginId) {
    return <OrePluginInfo> await transport.get();
  }
  
  return <Array<OrePluginInfo>> await transport.get(params);
}

export default async function info (pluginId?: string, cmd: InfoParams = {}): Promise<void> {
  const options: OreSearchParams = {
    categories: cmd.categories || [],
    sort: cmd.sort,
    q: cmd.query,
    limit: cmd.limit,
    offset: cmd.offset,
  };

  try {
    startSpinner('Resolving plugin(s)...');

    const results: InfoResponse = await getInfo(pluginId, options) as InfoResponse;

    log(beautifyInfo(results));
  } catch (error) {
    log(`Unable to retreive plugins. ${error.message}`);
  }
}