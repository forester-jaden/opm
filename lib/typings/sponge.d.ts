declare interface Build {
  version: string,
  dependencies: {
    forge?: string,
    minecraft: string,
    spongeapi: string
  }
}

declare interface SpongeInfo {
  name: string,
  pluginId: string,
  buildTypes: {
    bleeding: {
      latest: Build
    },
    stable: {
      latest: Build,
      recommended: Build,
    },
  },
  dependencies: {
    minecraft: Array<string>,
    sponge: Array<string>,
    spongeapi: Array<string>
  },
  github: {
    owner: string,
    repo: string,
  }
}

declare interface SpongeArtifact {
  md5: string,
  sha1: string,
  size: number,
  url: string
}

declare interface SpongeChange {
  author: string,
  date: string,
  description?: string,
  id: string,
  submodules?: {
    title: string,
    [key: string]: Array<SpongeChange> | string
  },
  title: string
}

declare interface SpongeDownload {
  artifacts: {
    '': SpongeArtifact,
    dev: SpongeArtifact,
    sources: SpongeArtifact
  },
  changelog?: {
    [id: number]: Array<SpongeChange>,
  },
  commit: string,
  dependencies: {
    minecraft: string,
    sponge: string,
    spongeapi: string
  },
  published: string,
  type: string,
  version: string
}

declare type SpongeDownloads = Array<SpongeDownload>