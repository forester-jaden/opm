declare interface OreSearchParams {
  categories?: Array<Number>,
  sort?: Number,
  q?: string,
  limit?: Number,
  offset?: Number,
}

declare interface OrePluginInfo {
  pluginId: string,
  createdAt: string,
  name: string,
  owner: string,
  description: string,
  href: string,
  members: [{
    userId: number,
    name: string,
    roles: Array<string>,
  }],
  channels: [{
    name: string,
    color: string,
    nonReviewed: boolean,
  }],
  recommended: OrePluginInfo,
  dependencies?: [{
    pluginId: string,
    version: string,
  }],
  downloads: number,
  fileSize?: number,
  md5?: string,
  staffApproved?: boolean,
  tags?: [{
    id: string,
    name: string,
    data: string,
    backgroundColor: string,
    foregroundColor: string,
  }],
  category?: {
    title: string,
    icon: string,
  },
  views?: number,
  stars?: number,
}

declare interface OreConfirmation {
  message: Array<string>,
  post: string,
  url: string,
  token: string
}
