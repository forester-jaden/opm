import { Spinner } from "cli-spinner";

const spinner = new Spinner('');

export function log(message: string, cancelSpinner: boolean = true): void {
  const shouldRestart: boolean = spinner.isSpinning() && !cancelSpinner;
  let attachment: string = '';
  if (spinner.isSpinning()) {
    spinner.stop();
    spinner.clearLine(spinner.stream);
    attachment = '\n';
  }

  console.log(message + attachment);

  if (shouldRestart) {
    spinner.start();
  }
}

export function startSpinner(title: string = 'Loading...', spinnerText: string = '|/-\\'): void {
  spinner.setSpinnerTitle(title + ' %s');
  spinner.setSpinnerString(spinnerText);
  spinner.start();
}

export function stopSpinner(): void {
  spinner.stop();
  spinner.clearLine(spinner.stream);
}
