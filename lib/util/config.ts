import * as fs from 'fs';
import * as path from 'path';
import { resolve } from 'dns';

let cached = false;
let config: Config = {
  launchProperties: {
    minRam: '512M',
    maxRam: '1G'
  }
};

export const configPath = './opm.config.json';

export interface Config {
  launchProperties: {
    minRam: string,
    maxRam: string
  },
  plugins?: {
    [name: string]: string
  }
}

export function getConfig(): Promise<Config> {
  const resolvedPath: string = path.resolve(process.cwd(), configPath);

  return new Promise<Config>((resolve, reject) => {
    if (cached) {
      return resolve(config);
    }

    if (!fs.existsSync(resolvedPath)) {
      return writeConfig();
    }

    return fs.readFile(configPath, { encoding: 'utf-8' }, (error, data) => {
      if (error) {
        return reject(new Error('Unable to read config.'));
      }

      config = JSON.parse(data);
      cached = true;
      return resolve(config);
    });
  })
}

export function writeConfig(): Promise<Config> {
  const resolvedPath: string = path.resolve(process.cwd(), configPath);

  return new Promise<Config>((resolve, reject) => {
    return fs.writeFile(resolvedPath, JSON.stringify(config, null, 2), {
      encoding: 'utf-8'
    }, (error) => {
      if (error) {
        return reject(new Error('Unable to write config.'));
      }

      cached = true;
      return resolve(config);
    });
  });
}

export async function addPlugin(name: string, version: string): Promise<void> {
  await getConfig();

  if (!config.plugins) {
    config.plugins = {};
  }

  config.plugins[name] = version;

  await writeConfig();
  return;
}

export function hasConfig(): boolean {
  return fs.existsSync(path.resolve(process.cwd(), configPath));
}
