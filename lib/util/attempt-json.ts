export default function attemptJson(json: string): object | string {
  try {
    return JSON.parse(json);
  } catch {
    return json;
  }
}
