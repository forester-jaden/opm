export default function list(items: string): Array<string> {
  return items.split(',');
}