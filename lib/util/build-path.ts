import * as path from 'path';
import * as fs from 'fs';
import mkdirp = require('mkdirp');

export default function buildPaths(fileName: string): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    const pathName: string = path.dirname(fileName);

    if (!fs.existsSync(pathName)) {
      mkdirp(pathName, (error) => {
        if (error) {
          return reject(new Error('Unable to create directory.'));
        }
        return resolve();
      });
    }

    return resolve();
  });
}
