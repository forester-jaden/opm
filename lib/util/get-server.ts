import * as glob from 'glob-promise';

export default async function getServer(): Promise<string | null> {
  return (await glob('sponge*.jar'))[0];
}